# Hough Forest
--------------

Hough Forest Code for Object Detection

**Class-Specific Hough Forests for Object Detection**

Source code copied and adapted from: http://www.iai.uni-bonn.de/~gall/projects/houghforest/houghforest.html

This Code is implemented by the respective authors for the following publication:

- Gall J., Yao A., Razavi N., van Gool L., and Lempitsky V., Hough Forests for Object Detection, Tracking, and Action Recognition (PDF), IEEE Transactions on Pattern Analysis and Machine Intelligence, Vol. 33, No. 11, 2188-2202, 2011. ©IEEE

- Gall J. and Lempitsky V., Class-Specific Hough Forests for Object Detection (PDF), IEEE Conference on Computer Vision and Pattern Recognition (CVPR'09), 2009. © IEEE 

The following additions have been made to the project to add the respective functionalities:

- Originally implemented to work on Linux, now prepared a Visual Studio Project so it can be run on Windows. 
- All respective dll and lib files for OpenCV have been added so it is a stand alone project, that works straight from the clone.
- Sh scripts for running the executables have been converted to BATCH scripts handling the Windows execution.

All the datasets provided by the author at the above link are included.

----------------
# Usage
## Quick way to use example data for training and testing
To run the project simple go to the DEBUG folder and execute the batch script files

- run.bat <--  runs the code with default options

- run_detect.bat <-- Object detection on the provided dataset

- run_train.bat <-- Training from positive and negative training samples

## Directly using the executable
To directly use the executable please open a command prompt window and browse to the folder containing the executable.

The following information is provided by the original authors on the usage of this code:

    Usage: hForest.exe mode [config.txt] [tree_offset]
    
    mode: 0 - train; 1 - show; 2 - detect
    tree_offset: output number for trees
    Load default: mode - 2 

-

-------
### Disclaimer
As part of the license of this source code I am including the original disclaimer which can also be found here: http://www.iai.uni-bonn.de/~gall/projects/houghforest/houghforest.html

 By installing, copying, or otherwise using this Software, you agree to be bound by the terms of the Microsoft Research Shared Source License Agreement (non-commercial use only). If you do not agree, do not install copy or use the Software. The Software is protected by copyright and other intellectual property laws and is licensed, not sold.

The software comes Ã¢â‚¬Å“as isÃ¢â‚¬Â, with no warranties. This means no express, implied or statutory warranty, including without limitation, warranties of merchantability or fitness for a particular purpose, any warranty against interference with your enjoyment of the software or any warranty of title or non-infringement. There is no warranty that this software will fulfill any of your particular purposes or needs. Also, you must pass this disclaimer on whenever you distribute the software or derivative works.

Neither microsoft nor any contributor to the software will be liable for any damages related to the software or this msr-ssla, including direct, indirect, special, consequential or incidental damages, to the maximum extent the law permits, no matter what legal theory it is based on. Also, you must pass this limitation of liability on whenever you distribute the software or derivative works.